﻿using UnityEngine;
using System.Collections;

public class PlayerController : Player
{
    bool didShot = false;
    bool shooting = false;

    string horizontal_input_left;
    string vertical_input_left;

    // Right axis to look around and aim
    string rotate_x_right;
    string rotate_y_right;

    string fire_axis;
    string run_button;

    void Start()
    {
        base.controller = GetComponent<CharacterController>();

#if UNITY_STANDALONE_OSX
        if (gameObject.name == "Player")
        {
            horizontal_input_left = "Horizontal";
            vertical_input_left = "Vertical";

            rotate_x_right = "RotateX";
            rotate_y_right = "RotateY";

            fire_axis = "Fire";
            run_button = "Run";
        }
        else if (gameObject.name == "Player 2")
        {
            horizontal_input_left = "Horizontal2";
            vertical_input_left = "Vertical2";

            rotate_x_right = "RotateX2";
            rotate_y_right = "RotateY2";

            fire_axis = "Fire2";
            run_button = "Run2";
        }
#endif

#if UNITY_STANDALONE_WIN
        if (gameObject.name == "Player")
        {
            horizontal_input_left = "Horizontal";
            vertical_input_left = "Vertical";

            rotate_x_right = "RotateX_Windows";
            rotate_y_right = "RotateY_Windows";

            fire_axis = "Fire_Windows";
            run_button = "Run_Windows";
        }
        else if (gameObject.name == "Player 2")
        {
            horizontal_input_left = "Horizontal2";
            vertical_input_left = "Vertical2";

            rotate_x_right = "RotateX2_Windows";
            rotate_y_right = "RotateY2_Windows";

            fire_axis = "Fire2_Windows";
            run_button = "Run2_Windows";
        }
#endif

#if UNITY_STANDALONE_LINUX
        if (gameObject.name == "Player")
        {
            horizontal_input_left = "Horizontal";
            vertical_input_left = "Vertical";

            rotate_x_right = "RotateX_Linux";
            rotate_y_right = "RotateY_Linux";

            fire_axis = "Fire_Linux";
            run_button = "Run_Linux";
        }
        else if (gameObject.name == "Player 2")
        {
            horizontal_input_left = "Horizontal2";
            vertical_input_left = "Vertical2";

            rotate_x_right = "RotateX2_Linux";
            rotate_y_right = "RotateY2_Linux";

            fire_axis = "Fire2_Linux";
            run_button = "Run2_Linux";
        }
#endif
    }

    void Update()
    {

        base.ManageAnim(base.controller.velocity.magnitude);

        // Run option
        if (Input.GetButton(run_button))
        {
            base.Run(Input.GetAxis(horizontal_input_left), Input.GetAxis(vertical_input_left));
        }
        else
        {
            base.Move(Input.GetAxis(horizontal_input_left), Input.GetAxis(vertical_input_left));
        }

        base.LookAround(Input.GetAxis(rotate_x_right), Input.GetAxis(rotate_y_right));

        if (base.controller.velocity.magnitude < 0.4f && !shooting)
        {
            base.gunRunning.SetActive(false);
            base.gunIdle.SetActive(true);
            base.gunAttacking.SetActive(false);
        }
        else if (base.controller.velocity.magnitude >= 0.5f)
        {
            base.gunRunning.SetActive(true);
            base.gunIdle.SetActive(false);
            base.gunAttacking.SetActive(false);
        }

#if UNITY_STANDALONE_LINUX
        if (Input.GetButton(fire_axis) && !didShot || Input.GetMouseButtonDown(0) && !didShot)
#endif
#if UNITY_STANDALONE_WIN
        if (Input.GetAxis(fire_axis) > 0 && !didShot || Input.GetMouseButtonDown(0) && !didShot)
#endif
#if UNITY_STANDALONE_OSX
        if (Input.GetAxis(fire_axis) > 0 && !didShot || Input.GetMouseButtonDown(0) && !didShot)
#endif
        {
            if (Time.time > base.fireRate + base.lastShot)
            {
                shooting = true;
                base.Shoot();
                StartCoroutine(GunFix());
                didShot = true;
                base.lastShot = Time.time;
            }
        }
#if UNITY_STANDALONE_OSX
        else if (Input.GetAxis(fire_axis) <= 0 || Input.GetMouseButtonDown(0)) {
#endif
#if UNITY_STANDALONE_WIN
        else if (Input.GetAxis(fire_axis) <= 0 || Input.GetMouseButtonDown(0)){
#endif
#if UNITY_STANDALONE_LINUX
        else if (Input.GetButtonUp(fire_axis) || Input.GetMouseButtonDown(0)){
#endif
            didShot = false;
        }

    }

    IEnumerator GunFix()
    {
        if (base.controller.velocity.magnitude >= 0.5f)
        {
            base.gunRunning.SetActive(true);
        }
        else
        {
            base.gunAttacking.SetActive(true);
        }
        base.gunIdle.SetActive(false);

        yield return new WaitForSeconds(1.2f);
        shooting = false;
    }
}