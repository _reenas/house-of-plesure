﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    // Moving variables
    private Vector3 moveDirection = Vector3.zero;
    public CharacterController controller;
    public float speed = 6.0f;
    public float speedMultiplier = 6.0f;
    public float gravity = 20.0f;

    // Looking variables
    public GameObject playerCamera;
    public float lookSensitivity = 5f;
    public float yRotation;
    public float xRotation;
    public float currentYRotation;
    public float currentXRotation;
    public float yRotationV;
    public float xRotationV;
    public float lookSmoothDamp = 0.06f;
    public float lookClamp;

    public GameObject waterGunFX;
    public GameObject gunIdle;
    public GameObject gunRunning;
    public GameObject gunAttacking;

    // Shooting variables
    public GameObject bulletPrefab;
    public GameObject weapon;
    public float holyWaterAmount = 1;

    public PlayerStats stats;

    public Animator animator;
    public float fireRate = 1f;
    public float lastShot = 0.0f;


    public void ManageAnim(float magnitude)
    {
        animator.SetFloat("Magnitude", magnitude);
    }

    public void Move(float _horizontal, float _vertical)
    {
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(_horizontal, 0.0f, _vertical);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        controller.Move(moveDirection * Time.deltaTime);

    }

    public void Run(float _horizontal, float _vertical)
    {
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(_horizontal, 0.0f, _vertical);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        controller.Move(moveDirection * speedMultiplier * Time.deltaTime);

    }

    public void LookAround(float _xRotation, float _yRotation)
    {
        yRotation += _yRotation * lookSensitivity;
        xRotation -= _xRotation * lookSensitivity;
        xRotation = Mathf.Clamp(xRotation, -lookClamp, lookClamp);
        currentXRotation = Mathf.SmoothDamp(currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
        currentYRotation = Mathf.SmoothDamp(currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);

        transform.localEulerAngles = new Vector3(currentXRotation, currentYRotation, 0);

        // transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, playerCamera.transform.localEulerAngles.y, transform.localEulerAngles.z);

        playerCamera.transform.rotation = Quaternion.Euler(currentXRotation, currentYRotation, 0);
    }

    public void ChangeScore()
    {

        stats._playerScore += 13;

        if (this.gameObject.name == "Player")
        {
            CanvasManager.instance.playerScore[0].GetComponent<TextMeshProUGUI>().SetText("Score : " + stats._playerScore.ToString());
        }
        else
        {
            CanvasManager.instance.playerScore[1].GetComponent<TextMeshProUGUI>().SetText("Score : " + stats._playerScore.ToString());
        }
    }

    public void Shoot()
    {
        if (holyWaterAmount >= 0)
        {
            animator.SetTrigger("Attack");
            GameObject waterFX = Instantiate(waterGunFX, weapon.transform);

            Destroy(waterFX, 0.5f);
            GameObject newBullet = Instantiate(bulletPrefab, weapon.transform);
            newBullet.GetComponent<Rigidbody>().AddForce(newBullet.transform.forward * 1000);

            holyWaterAmount -= 0.1f;

            if (this.gameObject.name == "Player")
            {
                CanvasManager.instance.playerHolyWater[0].GetComponent<Image>().fillAmount = holyWaterAmount;
            }
            else
            {
                CanvasManager.instance.playerHolyWater[1].GetComponent<Image>().fillAmount = holyWaterAmount;
            }
        }
    }

    void Refill()
    {
        if (holyWaterAmount < 1)
        {
            holyWaterAmount = 1;
        }
        if (this.gameObject.name == "Player")
        {
            CanvasManager.instance.playerHolyWater[0].GetComponent<Image>().fillAmount = holyWaterAmount;
        }
        else
        {
            CanvasManager.instance.playerHolyWater[1].GetComponent<Image>().fillAmount = holyWaterAmount;
        }
    }

    void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.CompareTag("WaterFill"))
        {
            // Debug.Log("fill");
            //Add text and butto for refill and maybe animation that it fills
            Refill();

        }
    }
}
