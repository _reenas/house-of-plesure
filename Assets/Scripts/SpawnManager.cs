﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject spawnArea;
    public GameObject[] enemyPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnEnemy()
    {
        Vector3 spawnBox = spawnArea.transform.localScale;
        Vector3 position = new Vector3(Random.value * spawnBox.x, Random.value * spawnBox.x, Random.value * spawnBox.x);
        position = transform.TransformPoint(position - spawnBox / 2);
        GameObject obj = Instantiate(enemyPrefabs[0], spawnArea.transform);
    }
}
