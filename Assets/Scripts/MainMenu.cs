﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Text flashingText;
    public GameObject fakeLoading;

    void Start()
    {
        StartCoroutine(BlinkText());
        fakeLoading.SetActive(false);
    }

    public IEnumerator BlinkText()
    {
        while (true)
        {
            flashingText.text = "";
            yield return new WaitForSeconds(.5f);
            flashingText.text = "PRESS ANY KEY TO START";
            yield return new WaitForSeconds(.5f);
        }
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            fakeLoading.SetActive(true);
            SceneManager.LoadSceneAsync(1);
        }
    }
}
