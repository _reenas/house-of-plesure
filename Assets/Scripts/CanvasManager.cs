﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager instance;
    public GameObject[] playerHealth;
    public GameObject[] playerHolyWater;
    public GameObject[] playerScore;

    public GameObject currentTime;
    public float passedTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        currentTime.GetComponent<TextMeshProUGUI>().SetText(Time.time.ToString("F1"));
    }

    void Reset()
    {
        for (int i = 0; i < 2; i++)
        {
            playerHealth[i].GetComponent<TextMeshProUGUI>().SetText("100");
            playerHolyWater[i].GetComponent<Image>().fillAmount = 1f;
            playerScore[i].GetComponent<TextMeshProUGUI>().SetText("Score : 0");
        }

        currentTime.GetComponent<TextMeshProUGUI>().SetText("00:00");
    }
}
