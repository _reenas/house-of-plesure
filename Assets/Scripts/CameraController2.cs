﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController2 : MonoBehaviour
{
    public float lookSensitivity = 5f;
    public float yRotation;
    public float xRotation;
    public float currentYRotation;
    public float currentXRotation;
    public float yRotationV;
    public float xRotationV;
    public float lookSmoothDamp = 0.06f;
    public float lookClamp;

    void Update()
    {
        yRotation += Input.GetAxis("RotateY2") * lookSensitivity;
        xRotation -= Input.GetAxis("RotateX2") * lookSensitivity;
        xRotation = Mathf.Clamp(xRotation, -lookClamp, lookClamp);
        currentXRotation = Mathf.SmoothDamp(currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
        currentYRotation = Mathf.SmoothDamp(currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);
        transform.rotation = Quaternion.Euler(currentXRotation, currentYRotation, 0);

    }
}
