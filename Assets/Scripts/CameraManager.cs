﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    public bool horizontalView = false;

    public Camera[] cameraArray;
    // Start is called before the first frame update
    void Start()
    {
        // ChangeSplitscreen();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeSplitscreen();
        }
    }

    public void ChangeSplitscreen()
    {
        horizontalView = !horizontalView;
        if (horizontalView)
        {
            // TODO: change this all
            // if(cameraArray.Length == 0) {
            //     return Debug.LogWarning("No Cameras");
            // }
            // else if(cameraArray.Length == 1)
            // {

            // }
            // if (cameraArray.Length == 2)
            // {
            cameraArray[0].rect = new Rect(0, 0.5f, 1, 0.5f);
            cameraArray[1].rect = new Rect(0, 0, 1, 0.5f);
            
            // }
        }
        else
        {
            cameraArray[0].rect = new Rect(0, 0, 0.5f, 1);
            cameraArray[1].rect = new Rect(0.5f, 0, 0.5f, 1);
        }
    }
}
