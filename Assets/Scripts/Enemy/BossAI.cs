﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class BossAI : MonoBehaviour
{
    public float lookRadius = 10f;
    public Transform target;
    NavMeshAgent agent;
    const float locomationAnimationSmoothTime = .1f;
    Animator animator;
    CharacterStats myStats;

    public float attackSpeed = 1f;
    private float attackCooldown = 0f;
    public float attackDelay = 0.6f;
    bool canAttack = true;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        myStats = GetComponent<CharacterStats>();
        animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        Move();
    }
    void Move()
    {
        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("Speed", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);

        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= lookRadius)
        {
            agent.SetDestination(target.position);

            if (distance <= agent.stoppingDistance)
            {
                CharacterStats targetStats = target.GetComponent<CharacterStats>();
                if (targetStats != null)
                {
                    Attack(targetStats);
                }
                FaceTarget();
            }
        }

        attackCooldown -= Time.deltaTime;
    }
    public void Attack(CharacterStats targetStats)
    {
        if (attackCooldown <= 0f && canAttack)
        {
            StartCoroutine(DoDamage(targetStats, attackDelay));
            attackCooldown = attackSpeed;
        }
    }

    IEnumerator DoDamage(CharacterStats stats, float delay)
    {
        canAttack = false;
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(delay / 2);
        stats.TakeDamage(myStats.damage.GetValue());

        if (target.name == "Player")
        {
            CanvasManager.instance.playerHealth[0].GetComponent<TextMeshProUGUI>().SetText(stats.currentHealth.ToString());
        }
        else
        {
            CanvasManager.instance.playerHealth[1].GetComponent<TextMeshProUGUI>().SetText(stats.currentHealth.ToString());
        }

        yield return new WaitForSeconds(delay / 2);
        canAttack = true;
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
