﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class EnemyAI : MonoBehaviour
{
    public float lookRadius = 10f;
    public Transform target;
    NavMeshAgent agent;
    const float locomationAnimationSmoothTime = .1f;
    Animator animator;

    public float attackSpeed = 1f;
    private float attackCooldown = 0f;
    public float attackDelay = 0.6f;
    bool canAttack = true;

    CharacterStats myStats;

    public Transform[] targets;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        myStats = GetComponent<CharacterStats>();
        animator = GetComponentInChildren<Animator>();
        targets[0] = GameObject.Find("Player").transform;
        targets[1] = GameObject.Find("Player 2").transform;
    }

    void Update()
    {
        if (inRange())
        {
            FindClosestTarget();
        }

        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("Speed", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);

        if (target != null)
        {
            float distance = Vector3.Distance(target.position, transform.position);

            if (distance <= lookRadius)
            {
                agent.SetDestination(target.position);

                if (distance <= agent.stoppingDistance)
                {
                    CharacterStats targetStats = target.GetComponent<CharacterStats>();
                    if (targetStats != null)
                    {
                        Attack(targetStats);
                    }
                    FaceTarget();
                }
            }
        }


        attackCooldown -= Time.deltaTime;
    }

    public void Attack(CharacterStats targetStats)
    {
        if (attackCooldown <= 0f && canAttack)
        {
            StartCoroutine(DoDamage(targetStats, attackDelay));
            attackCooldown = attackSpeed;
        }
    }

    IEnumerator DoDamage(CharacterStats stats, float delay)
    {
        canAttack = false;
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(delay / 2);
        stats.TakeDamage(myStats.damage.GetValue());

        if (target.name == "Player")
        {
            CanvasManager.instance.playerHealth[0].GetComponent<TextMeshProUGUI>().SetText(stats.currentHealth.ToString());
        }
        else
        {
            CanvasManager.instance.playerHealth[1].GetComponent<TextMeshProUGUI>().SetText(stats.currentHealth.ToString());
        }

        yield return new WaitForSeconds(delay / 2);
        canAttack = true;
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    void FindClosestTarget()
    {
        float first_target = Vector3.Distance(this.transform.position, targets[0].position);
        float second_target = Vector3.Distance(this.transform.position, targets[1].position);

        if (first_target > second_target)
        {
            target = targets[1];
        }
        if (first_target < second_target)
        {
            target = targets[0];
        }
    }

    bool inRange()
    {
        float first_target = Vector3.Distance(targets[0].position, transform.position);
        float second_target = Vector3.Distance(targets[1].position, transform.position);

        if (lookRadius >= first_target || lookRadius >= second_target)
        {
            return true;
        }
        return false;
    }
}
