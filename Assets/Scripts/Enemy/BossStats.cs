﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStats : CharacterStats
{
    public override void Die(string attacker = "")
    {
        base.Die();
        Destroy(gameObject);
    }
}
