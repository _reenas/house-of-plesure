﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth { get; set; }

    public Stat damage;

    public int _playerScore = 0;

    void Awake()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage, string attackerName = "")
    {
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Die(attackerName);
        }
    }

    public virtual void Die(string atacker = "")
    {
        GameObject.Find(atacker).GetComponent<PlayerController>().ChangeScore();
        
        Destroy(gameObject);
        
    }
}
