﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public int damageAmount = 10;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collider)
    {

        if (collider.transform.tag == "Enemy")
        {
            collider.gameObject.GetComponent<EnemyStats>().TakeDamage(damageAmount, GetShooter());
            Destroy(this.gameObject);
            // Debug.Log("Hit enemey");
        }

        // TODO: Add tag for ground
        if (collider.gameObject.name == "Plane")
        {
            Destroy(this.gameObject);
        }
    }

    string GetShooter() {
        string shooterName = transform.parent.transform.parent.gameObject.name;
        Debug.Log(shooterName);
        return shooterName;
    }
}
